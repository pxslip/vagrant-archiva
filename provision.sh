#!/usr/bin/env sh

ARCHIVA_VERSION=2.2.1
# first things first run apt-get update
apt-get update
# install wget to down
apt-get -y install wget unzip openjdk-7-jdk
# download archiva
wget --quiet http://apache.mirrors.tds.net/archiva/2.2.1/binaries/apache-archiva-$ARCHIVA_VERSION-bin.zip
# extract archiva
unzip -o apache-archiva-$ARCHIVA_VERSION-bin.zip
# move archiva to /opt
echo "Moving archiva to /opt"
mv apache-archiva-$ARCHIVA_VERSION /opt/apache-archiva
# link the archiva start script to init.d
echo "Linking the archiva start script to /etc/init.d/archiva"
ln -s /opt/apache-archiva/bin/archiva /etc/init.d/archiva
# make it so archiva starts on boot
echo "Setting archiva to start on boot"
update-rc.d archiva defaults 80
# and finally start archiva
echo "starting archiva"
service archiva start
